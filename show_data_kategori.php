<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT nama_kategori,id_kategori FROM kategori";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_kategori= array();
            while($kategori = mysqli_fetch_assoc($result)){
                array_push($data_kategori,$kategori);
            }
            echo json_encode($data_kategori);
        }
    }
?>