<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Music Library - Update Music List</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Music Library</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="music.php">Music List<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="genre.php">Genre</a>
                <a class="nav-item nav-link" href="kategori.php">Kategori</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Update Data Music</h4>
<?php foreach($db->editdata($_GET['id_lagu']) as $mem) : ?>
<form action="proses.php?aksi=m_update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id">ID</label>
            <input type="text" placeholder="Masukkan ID Lagu" id="id_lagu" name="id_lagu" readonly class="form-control" value="<?= $mem['id_lagu'] ?>" >
            
        </div>
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" placeholder="Masukkan Judul" id="judul" name="judul" class="form-control" required value="<?= $mem['judul'] ?>">
        </div>
        <div class="form-group">
            <label for="artist">Artist</label>
            <input type="text" placeholder="Masukkan Nama Penyanyi" id="artist" name="artist" class="form-control" required value="<?= $mem['artist'] ?>">
        </div>
        <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="text" placeholder="Masukkan Tahun" id="tahun" name="tahun" class="form-control" required value="<?= $mem['tahun'] ?>">
            <!--  -->
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <select class="form-control" name="genre">   
            <?php foreach ($db->ambilgenre() as $pg) : ?>
                <?php 
                
                $genreter=$pg['id_genre'];
                    if($mem['id_genre']==$pg['id_genre']){
                        $select="selected";
                    }else{
                        $select="";
                    } echo "<option  value='$genreter' $select>".$pg['nama_genre']."</option>";
                    
                ?>
            <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label for="kategori">Kategori</label>
            <select class="form-control" name="kategori">    
            <?php foreach ($db->ambilkategori() as $pk) : ?>
                <?php 
                $kateter=$pk['id_kategori'];
                    if($mem['id_kategori']==$pk['id_kategori']){
                        $select="selected";
                    }else{
                        $select="";
                    } echo "<option value='$kateter' $select>".$pk['nama_kategori']."</option>";
                ?>
            <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label for="gambar">Gambar</label><br>
            <img src="<?= $mem['url'] ?>" width="80px" height="100px" />
            <input type="hidden" id="gambar" name="gambar" value="<?= $mem['gambar'] ?>">
            <input type="file" class="form-control-file" id="file" name="file">
        </div>
        
        <div class="form-group">
            <label for="music">Music</label><br>
            <?= $mem['music'] ?><br>
            <input type="file" class="form-control-file" id="file2" name="file2">
        </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="music.php" class="btn btn-warning">Batal</a>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>