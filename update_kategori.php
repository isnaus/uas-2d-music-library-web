<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Music Library - Update Kategori</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Music Library</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link " href="music.php">Music List</a>
                <a class="nav-item nav-link " href="genre.php">Genre</a>
                <a class="nav-item nav-link active" href="kategori.php">Kategori <span class="sr-only">(current)</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Update Data Kategori</h4>
<?php foreach($db->editdakat($_GET['id_kategori']) as $ktg) : ?>
<form action="proses.php?aksi=k_update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_kategori">ID</label>
            <input type="text" placeholder="Masukkan ID Kategori" id="id_kategori" name="id_kategori" class="form-control"  readonly value="<?= $ktg['id_kategori'] ?>"required>
        </div>
        <div class="form-group">
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" placeholder="Masukkan Nama Kategori" id="nama_kategori" name="nama_kategori" class="form-control" value="<?= $ktg['nama_kategori'] ?>" required>
        </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="kategori.php" class="btn btn-warning">Batal</a>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>