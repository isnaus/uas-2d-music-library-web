<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT nama_genre FROM genre ORDER BY nama_genre asc";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_genre = array();
            while($nm_genre = mysqli_fetch_assoc($result)){
                array_push($nama_genre,$nm_genre);
            }
            echo json_encode($nama_genre);
        }
    }
?>