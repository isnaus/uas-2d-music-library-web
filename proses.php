<?php 
include 'koneksi.php';
$db = new database();

$aksi = $_GET['aksi'];
if($aksi == "m_insert"){
    $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    $tempname = $_FILES['file']['tmp_name'];
    $filename2 = "MS_".date("YmdHis").".".pathinfo(basename($_FILES["file2"]["name"]),PATHINFO_EXTENSION);
    $tempname2 = $_FILES['file2']['tmp_name'];
    $ukuran	= $_FILES['file2']['size'];

    $hasil=$db->insertdata($_POST['id_lagu'],$_POST['judul'],$_POST['artist'],$_POST['genre'],$_POST['kategori'],$_POST['tahun'],$tempname,$filename,$tempname2,$filename2);
    
    if($hasil['respon']=="sukses"){
        header("location:music.php?pesan=insertsuccess");
    }else{
        header("location:music.php?pesan=insertfailed");
    }
}elseif($aksi == "m_update"){
    $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    $tempname = $_FILES['file']['tmp_name'];
    $filename2 = "MS_".date("YmdHis").".".pathinfo(basename($_FILES["file2"]["name"]),PATHINFO_EXTENSION);
    $tempname2 = $_FILES['file2']['tmp_name'];
    $ukuran	= $_FILES['file2']['size'];

    $hasil=$db->updatedata($_POST['id_lagu'],$_POST['judul'],$_POST['artist'],$_POST['genre'],$_POST['kategori'],$_POST['tahun'],$tempname,$filename,$tempname2,$filename2);
    if($hasil['respon']=="sukses"){
        header("location:music.php?pesan=updatesuccess");
    }else{
        header("location:music.php?pesan=updatefailed");
    }
}elseif($aksi == "m_delete"){ 	
    $hasil=$db->deletedata($_GET['id_lagu']);
    if($hasil['respon']=="sukses"){
        header("location:music.php?pesan=deletesuccess");
    }else{
        header("location:music.php?pesan=deletefailed");
    }
}elseif($aksi == "g_insert"){
    $hasil=$db->insertdatag($_POST['id_genre'],$_POST['nama_genre']);
    
    if($hasil['respon']=="sukses"){
        header("location:genre.php?pesan=insertsuccess");
    }else{
        header("location:genre.php?pesan=insertfailed");
    }
}elseif($aksi == "g_update"){
    $hasil=$db->updatedatag($_POST['id_genre'],$_POST['nama_genre']);
    if($hasil['respon']=="sukses"){
        header("location:genre.php?pesan=updatesuccess");
    }else{
        header("location:genre.php?pesan=updatefailed");
    }
}elseif($aksi == "g_delete"){ 	
    $hasil=$db->deletedatag($_GET['id_genre']);
    if($hasil['respon']=="sukses"){
        header("location:genre.php?pesan=deletesuccess");
    }else{
        header("location:genre.php?pesan=deletefailed");
    }
}elseif($aksi == "k_insert"){
    $hasil=$db->insertdakat($_POST['id_kategori'],$_POST['nama_kategori']);
    
    if($hasil['respon']=="sukses"){
        header("location:kategori.php?pesan=insertsuccess");
    }else{
        header("location:kategori.php?pesan=insertfailed");
    }
}elseif($aksi == "k_update"){
    $hasil=$db->updatedakat($_POST['id_kategori'],$_POST['nama_kategori']);
    if($hasil['respon']=="sukses"){
        header("location:kategori.php?pesan=updatesuccess");
    }else{
        header("location:kategori.php?pesan=updatefailed");
    }
}elseif($aksi == "k_delete"){ 	
    $hasil=$db->deletedakat($_GET['id_kategori']);
    if($hasil['respon']=="sukses"){
        header("location:kategori.php?pesan=deletesuccess");
    }else{
        header("location:kategori.php?pesan=deletefailed");
    }
}
?>