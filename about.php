<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Music Library - About</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Music Library</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="about.php">About <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="music.php">Music List</a>
                <a class="nav-item nav-link" href="genre.php">Genre</a>
                <a class="nav-item nav-link" href="kategori.php">Kategori</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="card mt-3">
            <div class="card-header">
                About Page
            </div>
            <div class="card-body">
                <h5 class="card-title">Music Library</h5>
                <p class="card-text">Musik Hits Beragam Genre</p>
                Dibuat oleh :
                <ul>
                    <li>Fanny Bagus Ramadhan - 1931733111</li>
                    <li>Isna Uswatun Khasanah - 1931733093</li>
                </ul>
                
            </div>
        </div>
    </div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
