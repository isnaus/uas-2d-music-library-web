<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "uas_android",
        $con,
        $path = "images/",
        $path2 = "music/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }


        public function tampildata(){
            $sql = "SELECT l.id_lagu,l.judul,l.artist,g.nama_genre,k.nama_kategori,l.tahun, CONCAT('http://localhost/musiclibrary/images/',gambar) AS url,l.music
                    FROM lagu l, genre g, kategori k
                    WHERE l.id_genre = g.id_genre and l.id_kategori = k.id_kategori";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }


        public function insertdata($id_lagu,$nm,$artist,$genre,$kategori,$tahun,$imstr,$file,$imstr2,$file2){
            $judul=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into lagu(id_lagu, judul, artist,id_genre,id_kategori, tahun, gambar, music) values(
                '$id_lagu','$judul','$artist', '$genre','$kategori','$tahun','$file','$file2')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                // if ($ukuran< 1044070){
                    if(move_uploaded_file($imstr,$this->path.$file) == false || move_uploaded_file($imstr2,$this->path2.$file2) == false){
                        $sql = "delete from lagu where id_lagu='$id_lagu'";
                        mysqli_query($this->con,$sql);  
                        $hasil['respon']="gagal";
                        return $hasil;   
                    }else{
                        $hasil['respon']="sukses";
                        return $hasil;
                    }
                // }else{
                //     $hasil['respon']="besar";
                //     return $hasil;
                // }
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdata($id_lagu){         // fungsi mengambil data
            $sql = "SELECT l.id_lagu,l.judul,l.artist,l.id_genre,l.id_kategori,l.tahun, CONCAT('http://localhost/musiclibrary/images/',gambar) AS url,l.music
                    FROM lagu l
                    WHERE l.id_lagu = '$id_lagu'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedata($id_lagu,$nm,$artist,$genre,$kategori,$tahun,$imstr,$file,$imstr2,$file2){ // fungsi mengudapte data
            $judul=mysqli_real_escape_string($this->con,trim($nm));
            if($imstr=="" && $imstr2==""){
                $sql = "UPDATE lagu SET judul='$judul', artist='$artist', id_genre='$genre', id_kategori='$kategori', tahun='$tahun'
                where id_lagu='$id_lagu'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //update data sukses tanpa foto
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }elseif($imstr==""){
                if(move_uploaded_file($imstr2,$this->path2.$file2) == false ){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE lagu SET judul='$judul', artist='$artist', id_genre='$genre', id_kategori='$kategori', tahun='$tahun',music='$file2'
                            where id_lagu='$id_lagu'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }elseif($imstr2==""){
                if(move_uploaded_file($imstr,$this->path.$file) == false ){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE lagu SET judul='$judul', artist='$artist', id_genre='$genre', id_kategori='$kategori', tahun='$tahun',gambar='$file'
                            where id_lagu='$id_lagu'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }else{
                if(move_uploaded_file($imstr,$this->path.$file) == false || move_uploaded_file($imstr2,$this->path2.$file2) == false){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE lagu SET judul='$judul', artist='$artist', id_genre='$genre', id_kategori='$kategori', tahun='$tahun',gambar='$file',music='$file2'
                            where id_lagu='$id_lagu'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }
        }      
        public function deletedata($id_lagu){
            $sql = "SELECT gambar from lagu where id_lagu='$id_lagu'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $gambar = $data['gambar'];
                    unlink($this->path.$gambar);
                }
                $sql = "DELETE from lagu where id_lagu='$id_lagu'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }
        }


        public function ambilgenre(){
            $sql = "SELECT * FROM genre ORDER BY nama_genre";
            $datgenre = mysqli_query($this->con,$sql);
            while ($dg = mysqli_fetch_array($datgenre))
            {
                $hasilg[] = $dg;
            }
            return $hasilg;
        }
    

        
        public function insertdatag($id_genre,$nm){
            $nama_genre=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into genre(id_genre, nama_genre) values(
                '$id_genre','$nama_genre')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                    $hasil['respon']="sukses";
                    return $hasil;
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdatag($id_genre){         // fungsi mengambil data
            $sql = "SELECT g.id_genre,g.nama_genre
                    FROM genre g
                    WHERE g.id_genre = '$id_genre'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedatag($id_genre,$nm){ // fungsi mengudapte data
            $nama_genre=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "UPDATE genre SET nama_genre='$nama_genre'
                    where id_genre='$id_genre'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil;
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }      
        public function deletedatag($id_genre){
            $sql = "DELETE from genre where id_genre='$id_genre'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil; //delete data sukses
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        
        }
        
        public function ambilkategori(){
            $sql = "SELECT * FROM kategori ORDER BY nama_kategori";
            $datkategori = mysqli_query($this->con,$sql);
            while ($dk = mysqli_fetch_array($datkategori))
            {
                $hasilk[] = $dk;
            }
            return $hasilk;
        } 
        public function insertdakat($id_kategori,$nm){
            $nama_kategori=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into kategori(id_kategori, nama_kategori) values(
                '$id_kategori','$nama_kategori')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                    $hasil['respon']="sukses";
                    return $hasil;
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdakat($id_kategori){         // fungsi mengambil data
            $sql = "SELECT g.id_kategori,g.nama_kategori
                    FROM kategori g
                    WHERE g.id_kategori = '$id_kategori'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedakat($id_kategori,$nm){ // fungsi mengudapte data
            $nama_kategori=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "UPDATE kategori SET nama_kategori='$nama_kategori'
                    where id_kategori='$id_kategori'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil;
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }      
        public function deletedakat($id_kategori){
            $sql = "DELETE from kategori where id_kategori='$id_kategori'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil; //delete data sukses
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        
        }
    }
?>