<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT l.id_lagu,l.judul,l.artist,l.tahun,concat('http://192.168.43.13/musiclibrary/images/',gambar) as url,g.nama_genre,k.nama_kategori
        FROM lagu l, genre g, kategori k
        WHERE l.id_genre = g.id_genre && l.id_kategori = k.id_kategori";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_lagu = array();
            while($lagu = mysqli_fetch_assoc($result)){
                array_push($data_lagu,$lagu);
            }
            echo json_encode($data_lagu);
        }
    }
?>