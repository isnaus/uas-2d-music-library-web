<?php
    $DB_NAME = "uas_android";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array(); $respon['kode'] = '000';
        switch($mode){
            case "insert":
                $id_lagu = $_POST["id_lagu"];
                $judul = mysqli_real_escape_string($conn,trim($_POST["judul"]));
                $nama_kategori = $_POST["nama_kategori"];
                $nama_genre = $_POST["nama_genre"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$artist = $_POST["artist"];
				$tahun = $_POST["tahun"];
                $path = "images/";

                $sql = "SELECT id_kategori from kategori where nama_kategori='$nama_kategori'";
                $sql1 = "SELECT id_genre from genre where nama_genre='$nama_genre'";

                $result = mysqli_query($conn,$sql);
                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_kategori = $data['id_kategori'];
                    $id_genre = $data1['id_genre'];

                    $sql = "INSERT into lagu(id_lagu, judul, artist, id_kategori, gambar, tahun, id_genre) values(
                        '$id_lagu','$judul','$artist','$id_kategori','$file', '$tahun', '$id_genre')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $sql = "delete from lagu where id_lagu='$id_lagu'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            echo json_encode($respon); exit(); //insert data sukses semua
                        }
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
            case "update":
                $id_lagu = $_POST["id_lagu"];
                $judul = mysqli_real_escape_string($conn,trim($_POST["judul"]));
                $nama_kategori = $_POST["nama_kategori"];
                $nama_genre = $_POST["nama_genre"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$artist = $_POST["artist"];
				$tahun = $_POST["tahun"];
                $path = "images/";


                $sql = "SELECT id_kategori from kategori where nama_kategori='$nama_kategori'";
                $sql1 = "SELECT id_genre from genre where nama_genre='$nama_genre'";

                $result = mysqli_query($conn,$sql);
                $result1 = mysqli_query($conn,$sql1);

        
                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_kategori = $data['id_kategori'];
                    $id_genre = $data1['id_genre'];

                    $sql = "";
                    if($imstr==""){
                        $sql = "UPDATE lagu SET judul='$judul',artist='$artist',id_kategori='$id_kategori', tahun='$tahun', id_genre='$id_genre'
                        where id_lagu='$id_lagu'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }else{
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            $sql = "UPDATE lagu SET judul='$judul',artist='$artist',id_kategori='$id_kategori',gambar='$file', tahun='$tahun', id_genre='$id_genre'
                                    where id_lagu='$id_lagu'";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                echo json_encode($respon); exit(); //update data sukses semua
                            }else{
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }
                        }
                    }
                }
            break;
            case "delete":
                $id_lagu = $_POST["id_lagu"];
                $sql = "SELECT gambar from lagu where id_lagu='$id_lagu'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['gambar'];
                        $path = "images/";
                        unlink($path.$photos);
                    }
                    $sql = "DELETE from lagu where id_lagu='$id_lagu'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit(); //delete data sukses
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
        }
    }
?>