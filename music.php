<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Music Library - Music List</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Music Library</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="music.php">Music List<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="genre.php">Genre</a>
                <a class="nav-item nav-link" href="kategori.php">Kategori</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Lagu</h4> 
    <a href="insert.php" class="btn btn-primary mb-3">
    Tambah Lagu
    </a>
    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "Sukses di Tambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "Sukses di Edit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "Sukses di Hapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "Gagal di Tambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "Gagal di Edit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "Gagal di Hapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data Music  <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">ID</th>
        <th scope="col">Judul</th>
        <th scope="col">Artist</th>
        <th scope="col">Genre</th>
        <th scope="col">Kategori</th>
        <th scope="col">Tahun</th>
        <th scope="col">Gambar</th>
        <th scope="col">Lagu</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db->tampildata() as $lgu) : ?>
        <tr>
            <td><?= $lgu['id_lagu'] ?></td>
            <td><?= $lgu['judul'] ?></td>
            <td><?= $lgu['artist'] ?></td>
            <td><?= $lgu['nama_genre']?></td>
            <td><?= $lgu['nama_kategori']?></td>
            <td><?= $lgu['tahun']?></td>
            <td><img src="<?= $lgu['url'] ?>" width="50px" height="50px" /></td>
            <td><?= $lgu['music']?></td>
            <td>
                <a href="update.php?id_lagu=<?php echo $lgu['id_lagu']; ?>" class="btn btn-warning">Edit</a>
                <a href="proses.php?id_lagu=<?php echo $lgu['id_lagu']; ?>&aksi=m_delete" class="btn btn-danger">Hapus</a>
			</td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>